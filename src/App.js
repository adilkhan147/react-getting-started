import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import NavBar from './components/nav-bar'
import Sidebar from './components/sidebar'
import Footer from './components/footer';
import './App.css';
import DataTable from "./components/dataTable";
import {Route} from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <div className="App">

                <NavBar/>

                <div id="wrapper">

                    <Sidebar/>
                    <Route exact path="/" render={() => (
                        <div id="content-wrapper">
                            <div className="container-fluid">
                                {/* Breadcrumbs*/}
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li className="breadcrumb-item active">Overview</li>
                                </ol>
                                {/* Icon Cards*/}
                                <div className="row">
                                    <div className="col-xl-3 col-sm-6 mb-3">
                                        <div className="card text-white bg-primary o-hidden h-100">
                                            <div className="card-body">
                                                <div className="card-body-icon">
                                                    <i className="fas fa-fw fa-comments"/>
                                                </div>
                                                <div className="mr-5">26 New Messages!</div>
                                            </div>
                                            <a className="card-footer text-white clearfix small z-1" href="#">
                                                <span className="float-left">View Details</span>
                                                <span className="float-right">
                        <i className="fas fa-angle-right"/>
                      </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-sm-6 mb-3">
                                        <div className="card text-white bg-warning o-hidden h-100">
                                            <div className="card-body">
                                                <div className="card-body-icon">
                                                    <i className="fas fa-fw fa-list"/>
                                                </div>
                                                <div className="mr-5">11 New Tasks!</div>
                                            </div>
                                            <a className="card-footer text-white clearfix small z-1" href="#">
                                                <span className="float-left">View Details</span>
                                                <span className="float-right">
                        <i className="fas fa-angle-right"/>
                      </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-sm-6 mb-3">
                                        <div className="card text-white bg-success o-hidden h-100">
                                            <div className="card-body">
                                                <div className="card-body-icon">
                                                    <i className="fas fa-fw fa-shopping-cart"/>
                                                </div>
                                                <div className="mr-5">123 New Orders!</div>
                                            </div>
                                            <a className="card-footer text-white clearfix small z-1" href="#">
                                                <span className="float-left">View Details</span>
                                                <span className="float-right">
                        <i className="fas fa-angle-right"/>
                      </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-sm-6 mb-3">
                                        <div className="card text-white bg-danger o-hidden h-100">
                                            <div className="card-body">
                                                <div className="card-body-icon">
                                                    <i className="fas fa-fw fa-life-ring"/>
                                                </div>
                                                <div className="mr-5">13 New Tickets!</div>
                                            </div>
                                            <a className="card-footer text-white clearfix small z-1" href="#">
                                                <span className="float-left">View Details</span>
                                                <span className="float-right">
                        <i className="fas fa-angle-right"/>
                      </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                {/* Area Chart Example*/}
                                <div className="card mb-3">
                                    <div className="card-header">
                                        <i className="fas fa-chart-area"/>
                                        Area Chart Example
                                    </div>
                                    <div className="card-body">
                                        <canvas id="myAreaChart" width="100%" height={30}/>
                                    </div>
                                    <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                                </div>
                                {/* DataTables Example */}
                                <div className="card mb-3">
                                    <div className="card-header">
                                        <i className="fas fa-table"/>
                                        Data Table Example
                                    </div>
                                    <div className="card-body">
                                        <DataTable/>
                                    </div>
                                    <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                                </div>
                            </div>
                            {/* /.container-fluid */}
                            {/* Sticky Footer */}
                            <Footer/>
                        </div>
                    )}/>
                    <Route path="/login" component={Footer}/>
                </div>

            </div>
        );
    }
}

export default App;
