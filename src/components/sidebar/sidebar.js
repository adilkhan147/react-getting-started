import React, {Component} from 'react'
import {Link} from 'react-router-dom';

class Sidebar extends Component {

    render(){
        return(
            <div>
                <ul className="sidebar navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="index.html">
                            <i className="fas fa-fw fa-tachometer-alt">
                                <span>Dashboard</span>
                            </i></a><i className="fas fa-fw fa-tachometer-alt">
                    </i></li>
                    <i className="fas fa-fw fa-tachometer-alt">
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="fas fa-fw fa-folder">
                                    <span>Pages</span>
                                </i></a><i className="fas fa-fw fa-folder">
                            <div className="dropdown-menu" aria-labelledby="pagesDropdown">
                                <h6 className="dropdown-header">Login Screens:</h6>
                                <Link to='/login'/>
                                <a className="dropdown-item" href="login.html">Login</a>
                                <a className="dropdown-item" href="register.html">Register</a>
                                <a className="dropdown-item" href="forgot-password.html">Forgot Password</a>
                                <div className="dropdown-divider">
                                    <h6 className="dropdown-header">Other Pages:</h6>
                                    <a className="dropdown-item" href="404.html">404 Page</a>
                                    <a className="dropdown-item" href="blank.html">Blank Page</a>
                                </div>
                            </div>
                        </i></li>
                        <i className="fas fa-fw fa-folder">
                            <li className="nav-item">
                                <Link to='/login' className="nav-link">
                                    <i className="fas fa-fw fa-chart-area">
                                        <span>Charts</span></i>
                                </Link>
                               </li>
                            <i className="fas fa-fw fa-chart-area">
                                <li className="nav-item">
                                    <a className="nav-link" href="tables.html">
                                        <i className="fas fa-fw fa-table">
                                            <span>Tables</span></i></a><i className="fas fa-fw fa-table">
                                </i></li>
                                <i className="fas fa-fw fa-table">
                                </i></i></i></i></ul>
                <i className="fas fa-fw fa-tachometer-alt"><i className="fas fa-fw fa-folder"><i
                    className="fas fa-fw fa-chart-area"><i className="fas fa-fw fa-table">
                </i></i></i></i></div>
        )
    }
}

export default Sidebar;